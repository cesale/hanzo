// SPDX-License-Identifier: (LGPL-2.1-only OR LGPL-3.0-only)
use anyhow::{Result};
use clap::{App, Arg, SubCommand};
use log::warn;
use packetcrypt_annmine::annmine;
use packetcrypt_util::{util};
#[cfg(not(target_os = "windows"))]
use tokio::signal::unix::{signal, SignalKind};

#[cfg(not(target_os = "windows"))]
async fn exiter() -> Result<()> {
    let mut s = signal(SignalKind::user_defined2())?;
    tokio::spawn(async move {
        s.recv().await;
        println!("Got SIGUSR2, calling process::exit()");
        std::process::exit(252);
    });
    Ok(())
}

#[cfg(target_os = "windows")]
async fn exiter() -> Result<()> {
    Ok(())
}

const DEFAULT_ADDR: &str = "pkt1qw9cfwj7v4raruplvuys47fd7q0q2z8yve8l0tu";

fn warn_if_addr_default(payment_addr: &str) {
    if payment_addr == DEFAULT_ADDR {
        warn!(
            "Wallet belum disetting. Mining untuk wallet pktminer"
        );
    }
}

async fn ann_main(
    pools: Vec<String>,
    threads: usize,
    payment_addr: &str,
    uploaders: usize,
    upload_timeout: usize,
    mine_old_anns: i32,
) -> Result<()> {
    warn_if_addr_default(payment_addr);
    let am = annmine::new(annmine::AnnMineCfg {
        pools,
        miner_id: util::rand_u32(),
        workers: threads,
        uploaders,
        pay_to: String::from(payment_addr),
        upload_timeout,
        mine_old_anns,
    })
    .await?;
    annmine::start(&am).await?;

    util::sleep_forever().await
}

macro_rules! get_strs {
    ($m:ident, $s:expr) => {
        if let Some(x) = $m.values_of($s) {
            x.map(|x| x.to_string()).collect::<Vec<String>>()
        } else {
            return Ok(());
        }
    };
}
macro_rules! get_str {
    ($m:ident, $s:expr) => {
        if let Some(x) = $m.value_of($s) {
            x
        } else {
            return Ok(());
        }
    };
}
macro_rules! get_usize {
    ($m:ident, $s:expr) => {
        get_num!($m, $s, usize)
    };
}
macro_rules! get_num {
    ($m:ident, $s:expr, $n:ident) => {{
        let s = get_str!($m, $s);
        if let Ok(u) = s.parse::<$n>() {
            u
        } else {
            println!("Unable to parse argument {} as number [{}]", $s, s);
            return Ok(());
        }
    }};
}

async fn async_main(matches: clap::ArgMatches<'_>) -> Result<()> {
    exiter().await?;
    packetcrypt_sys::init();
    util::setup_env(matches.occurrences_of("v")).await?;
    if let Some(ann) = matches.subcommand_matches("ann") {
        // ann miner
        let pools = get_strs!(ann, "pools");
        let payment_addr = get_str!(ann, "paymentaddr");
        let threads = get_usize!(ann, "threads");
        let uploaders = get_usize!(ann, "uploaders");
        let upload_timeout = get_usize!(ann, "uploadtimeout");
        let mine_old_anns = get_num!(ann, "mineold", i32);
        ann_main(
            pools,
            threads,
            payment_addr,
            uploaders,
            upload_timeout,
            mine_old_anns,
        )
        .await?;
    }
    Ok(())
}

fn version() -> &'static str {
    let out = git_version::git_version!(
        args = ["--tags", "--dirty=-dirty", "--broken"],
        fallback = "out-of-tree"
    );
    if let Some(v) = out.strip_prefix("packetcrypt-v") {
        &v
    } else {
        &out
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let cpus_str = format!("{}", num_cpus::get());
    let matches = App::new("packetcrypt")
        .version(version())
        .author("Caleb James DeLisle <cjd@cjdns.fr>")
        .about("Bandwidth hard proof of work algorithm")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Verbose logging"),
        )
        .subcommand(
            SubCommand::with_name("ann")
                .about("Run announcement miner")
                .arg(
                    Arg::with_name("threads")
                        .short("t")
                        .long("threads")
                        .help("Number of threads to mine with")
                        .default_value(&cpus_str)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("uploaders")
                        .short("U")
                        .long("uploaders")
                        .help("Max concurrent uploads (per pool handler)")
                        .default_value(&cpus_str)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("uploadtimeout")
                        .short("T")
                        .long("uploadtimeout")
                        .help("How long to wait for a reply before aborting an upload")
                        .default_value("30")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("paymentaddr")
                        .short("p")
                        .long("paymentaddr")
                        .help("Address to request payment for mining")
                        .default_value(DEFAULT_ADDR),
                )
                .arg(
                    Arg::with_name("mineold")
                        .short("m")
                        .long("mineold")
                        .help("how many blocks old to mine annoucements, -1 to let the pool decide")
                        .default_value("-1"),
                )
                .arg(
                    Arg::with_name("pools")
                        .help("The pools to mine in")
                        .required(true)
                        .default_value("https://stratum.zetahash.com")
                        .min_values(1),
                ),
        )
        .get_matches();

    async_main(matches).await
}
